export deafult {
  'branches': 'release',
  'ci': false,
  'plugins': [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/gitlab',
  ],
};
